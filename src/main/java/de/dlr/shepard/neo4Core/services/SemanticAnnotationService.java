package de.dlr.shepard.neo4Core.services;

import java.util.List;

import de.dlr.shepard.exceptions.InvalidBodyException;
import de.dlr.shepard.neo4Core.dao.BasicEntityDAO;
import de.dlr.shepard.neo4Core.dao.SemanticAnnotationDAO;
import de.dlr.shepard.neo4Core.dao.SemanticRepositoryDAO;
import de.dlr.shepard.neo4Core.entities.SemanticAnnotation;
import de.dlr.shepard.neo4Core.entities.SemanticRepository;
import de.dlr.shepard.neo4Core.io.SemanticAnnotationIO;
import de.dlr.shepard.semantics.SemanticRepositoryConnectorFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SemanticAnnotationService {

	private SemanticAnnotationDAO semanticAnnotationDAO = new SemanticAnnotationDAO();
	private SemanticRepositoryDAO semanticRepositoryDAO = new SemanticRepositoryDAO();
	private BasicEntityDAO basicEntityDAO = new BasicEntityDAO();
	private SemanticRepositoryConnectorFactory semanticRepositoryConnectorFactory = new SemanticRepositoryConnectorFactory();

	public List<SemanticAnnotation> getAllAnnotations(long entityId) {
		return semanticAnnotationDAO.findAllSemanticAnnotations(entityId);
	}

	public SemanticAnnotation getAnnotation(long id) {
		var annotation = semanticAnnotationDAO.find(id);
		if (annotation == null) {
			log.error("Semantic Annotation with id {} is null or deleted", id);
			return null;
		}
		return annotation;
	}

	public SemanticAnnotation createAnnotation(long entityId, SemanticAnnotationIO annotationIO) {
		var entity = basicEntityDAO.find(entityId);
		if (entity == null || entity.isDeleted())
			throw new InvalidBodyException("invalid entity");

		var propertyRepository = getRepository(annotationIO.getPropertyRepositoryId());
		var valueRepository = getRepository(annotationIO.getValueRepositoryId());
		var name = String.join("::", validateTerm(propertyRepository, annotationIO.getPropertyIRI()),
				validateTerm(valueRepository, annotationIO.getValueIRI()));

		var toCreate = new SemanticAnnotation();
		toCreate.setName(name);
		toCreate.setPropertyIRI(annotationIO.getPropertyIRI());
		toCreate.setValueIRI(annotationIO.getValueIRI());
		toCreate.setPropertyRepository(propertyRepository);
		toCreate.setValueRepository(valueRepository);

		var created = semanticAnnotationDAO.createOrUpdate(toCreate);

		entity.addAnnotation(created);
		basicEntityDAO.createOrUpdate(entity);

		return created;
	}

	public boolean deleteAnnotation(long id) {
		var result = semanticAnnotationDAO.delete(id);
		return result;
	}

	private SemanticRepository getRepository(long id) {
		var repository = semanticRepositoryDAO.find(id);
		if (repository == null || repository.isDeleted())
			throw new InvalidBodyException("invalid repository");

		return repository;
	}

	private String validateTerm(SemanticRepository repository, String iri) {
		var src = semanticRepositoryConnectorFactory.getRepositoryService(repository.getType(),
				repository.getEndpoint());
		var term = src.getTerm(iri);
		if (term == null || term.isEmpty())
			throw new InvalidBodyException("term could not be found");
		// Prefer the default label
		if (term.containsKey(""))
			return term.get("");
		// Then prefer the English label
		if (term.containsKey("en"))
			return term.get("en");
		// Fall back to the first label in the list
		return term.values().iterator().next();
	}

}
