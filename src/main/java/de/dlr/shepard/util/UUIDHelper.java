package de.dlr.shepard.util;

import java.util.UUID;

public class UUIDHelper {

	public UUID getUUID() {
		return UUID.randomUUID();
	}

}
