image: maven:3-eclipse-temurin-17

variables:
  MAVEN_CLI_OPTS: "--batch-mode"
  MAVEN_OPTS: "-Dmaven.repo.local=${CI_PROJECT_DIR}/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn"
  REPO: shepard
  IMAGE: backend
  DOCKER_IMAGE: "${CI_REGISTRY_IMAGE}:latest"

cache:
  - key: ${CI_COMMIT_REF_SLUG}
    paths:
      - .m2/repository/
  - key: ${CI_COMMIT_SHORT_SHA}
    paths:
      - target/

include:
  - template: Security/Secret-Detection.gitlab-ci.yml
  - local: .gitlab/ci/*.gitlab-ci.yml

stages:
  - test
  - build
  - build_clients
  - upload
  - deploy

.version_number:
  script:
    - export VERSION_NUMBER=$(echo ${CI_PIPELINE_CREATED_AT} | cut -d'T' -f 1 | tr - .)
    - export VERSION_NUMBER_DEV=${VERSION_NUMBER}-dev-${CI_PIPELINE_IID}-SNAPSHOT

.only_branches:
  only:
    - branches

.only_main:
  only:
    - main

.only_releases:
  only:
    - /^.*-release$/

format-prettier:
  extends: .only_branches
  stage: test
  image: node:20-slim
  before_script:
    - npm install --save-dev --save-exact prettier
  script:
    - npx prettier --check .

test:
  extends: .only_branches
  stage: test
  before_script:
    - !reference [.version_number, script]
  script:
    - mvn ${MAVEN_CLI_OPTS} -Drevision=${VERSION_NUMBER_DEV} -DskipITs verify -P test
    - cat target/site/jacoco/index.html | grep -oP 'Total.*?([0-9]{1,3})%'
    - ls -hal target
  coverage: "/Total.*?([0-9]{1,3})%/"
  artifacts:
    when: always
    expire_in: 2 days
    paths:
      - target/jacoco.exec
      - target/surefire-reports/*
      - target/site/jacoco/*
    reports:
      junit:
        - target/surefire-reports/*.xml

integration-test:
  extends: .only_branches
  stage: test
  variables:
    NEO4J_AUTH: neo4j/shepard
    MONGO_INITDB_ROOT_USERNAME: mongo
    MONGO_INITDB_ROOT_PASSWORD: shepard
    MONGO_INITDB_DATABASE: test
    INFLUXDB_ADMIN_USER: influx
    INFLUXDB_ADMIN_PASSWORD: shepard
  services:
    - name: neo4j:4.4
      alias: neo4j
    - name: mongo:4.4
      alias: mongodb
    - name: influxdb:1.8
      alias: influxdb
  before_script:
    - !reference [.version_number, script]
  script:
    - mvn ${MAVEN_CLI_OPTS} -Drevision=${VERSION_NUMBER_DEV} -DskipUTs verify -P test
    - ls -hal target
  artifacts:
    when: always
    expire_in: 2 days
    paths:
      - target/tomcat10x-logs/*
      - target/failsafe-reports/*
    reports:
      junit:
        - target/failsafe-reports/*.xml

site:
  extends: .only_branches
  stage: build
  needs: [test]
  dependencies: [test]
  before_script:
    - !reference [.version_number, script]
  script:
    - ls -hal target
    - mvn ${MAVEN_CLI_OPTS} -Drevision=${VERSION_NUMBER_DEV} site -P test
    - ls -hal target/site
  artifacts:
    when: always
    expire_in: 2 days
    paths:
      - target/site/*

coverage:
  extends: .only_branches
  stage: build
  image: haynes/jacoco2cobertura:1.0.9
  needs: [test]
  dependencies: [test]
  script:
    - ls -hal target/site/jacoco
    # convert report from jacoco to cobertura
    - python /opt/cover2cover.py target/site/jacoco/jacoco.xml src/main/java > target/site/cobertura.xml
    # read the <source></source> tag and prepend the path to every filename attribute
    - python /opt/source2filename.py target/site/cobertura.xml
  artifacts:
    expire_in: 2 days
    reports:
      coverage_report:
        coverage_format: cobertura
        path: target/site/cobertura.xml

build:
  stage: build
  before_script:
    - !reference [.version_number, script]
    - echo ${VERSION_NUMBER_DEV}
  script:
    - mvn ${MAVEN_CLI_OPTS} -Drevision=${VERSION_NUMBER_DEV} -DskipTests package -P prod
  artifacts:
    when: always
    expire_in: 2 days
    paths:
      - target/shepard.war
      - target/openapi/*

openapi-diff:
  extends: .only_branches
  stage: build_clients
  image:
    name: openapitools/openapi-diff:2.0.1
    entrypoint: [""]
  needs: [build]
  dependencies: [build]
  script:
    - java -jar /app/openapi-diff.jar https://gitlab.com/dlr-shepard/backend/-/jobs/artifacts/main/raw/target/openapi/openapi.yaml?job=build target/openapi/openapi.yaml --fail-on-changed --text target/openapi/diff.txt --markdown target/openapi/diff.md
  artifacts:
    when: always
    expire_in: 2 days
    paths:
      - target/openapi/*
  allow_failure: true

.upload_docker: &upload_docker
  image: docker:24.0
  stage: upload
  needs: [build]
  dependencies: [build]
  services:
    - name: docker:dind

upload_docker_dev:
  <<: *upload_docker
  extends: .only_main
  before_script:
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" ${CI_REGISTRY}
    - !reference [.version_number, script]
    - export VERSION_NUMBER=${VERSION_NUMBER}-dev.${CI_PIPELINE_IID}
    - echo ${VERSION_NUMBER}
  script:
    - docker build -t ${CI_REGISTRY_IMAGE}:${VERSION_NUMBER} -t ${CI_REGISTRY_IMAGE}:dev .
    - docker push ${CI_REGISTRY_IMAGE}:${VERSION_NUMBER}
    - docker push ${CI_REGISTRY_IMAGE}:dev

upload_docker:
  <<: *upload_docker
  extends: .only_releases
  before_script:
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" ${CI_REGISTRY}
    - !reference [.version_number, script]
    - echo ${VERSION_NUMBER}
  script:
    - docker build -t ${CI_REGISTRY_IMAGE}:${VERSION_NUMBER} -t ${CI_REGISTRY_IMAGE}:latest .
    - docker push ${CI_REGISTRY_IMAGE}:${VERSION_NUMBER}
    - docker push ${CI_REGISTRY_IMAGE}:latest

pages:
  extends: .only_main
  stage: upload
  needs: [site]
  dependencies: [site]
  script:
    - ls -hal target/site
    - mkdir public
    - mv target/site/* public/
    - echo ${CI_PAGES_DOMAIN}
  artifacts:
    expire_in: 2 days
    paths:
      - public
